import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './common/guard/auth.guard';
import { AccoutsComponent } from './main-module/pages/account/accouts/accouts.component';
import { CreateAccountComponent } from './main-module/pages/account/create-account/create-account.component';
import { UpdateAccountComponent } from './main-module/pages/account/update-account/update-account.component';
import { BanksComponent } from './main-module/pages/bank/banks/banks.component';
import { CreateBankComponent } from './main-module/pages/bank/create-bank/create-bank.component';
import { UpdateBankComponent } from './main-module/pages/bank/update-bank/update-bank.component';
import { HomeComponent } from './main-module/pages/home/home.component';
import { FooterComponent } from './main-module/shared-component/footer/footer.component';
import { HeaderComponent } from './main-module/shared-component/header/header.component';
import { MenuComponent } from './main-module/shared-component/menu/menu.component';
import { SignInComponent } from './user-module/pages/sign-in/sign-in.component';
import { SignUpComponent } from './user-module/pages/sign-up/sign-up.component';



const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'sign-in', component: SignInComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: 'account', component: AccoutsComponent, canActivate: [AuthGuard] },
  { path: 'account/create', component: CreateAccountComponent, canActivate: [AuthGuard] },
  { path: 'account/update/:id', component: UpdateAccountComponent, canActivate: [AuthGuard] },
  { path: 'bank', component: BanksComponent, canActivate: [AuthGuard] },
  { path: 'bank/create', component: CreateBankComponent, canActivate: [AuthGuard] },
  { path: 'bank/update/:id', component: UpdateBankComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const RoutingModule = [
  FooterComponent,
  HeaderComponent,
  MenuComponent,
  HomeComponent,
  SignInComponent,
  SignUpComponent,
  AccoutsComponent,
  CreateAccountComponent,
  UpdateAccountComponent,
  BanksComponent,
  CreateBankComponent,
  UpdateBankComponent

];