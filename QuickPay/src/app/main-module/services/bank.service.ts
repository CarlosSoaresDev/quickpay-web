import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Bank } from '../models/bank';

@Injectable({ providedIn: 'root' })
export class BankService {

    constructor(
        private http: HttpClient
    ) { }

    getAll() {
        return this.http
            .get<Bank[]>(`${environment.URL_API}/Bank`)
    }

    getByID() {
        return this.http
            .get<Bank>(`${environment.URL_API}/Bank`)
    }

    create(bank: Bank) {
        return this.http
            .post<number>(`${environment.URL_API}/Bank`, bank);
    }

    update(bankID: number, bank: Bank) {
        return this.http
            .put<number>(`${environment.URL_API}/Bank/${bankID}`, bank);
    }

    delete(bankID: number) {
        return this.http
            .delete(`${environment.URL_API}/Bank/${bankID}`);
    }
}
