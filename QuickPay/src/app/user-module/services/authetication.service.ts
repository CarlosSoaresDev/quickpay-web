import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';
import { Person } from '../models/person';
import { Token } from '../models/token';
import { JwtHelperService } from '@auth0/angular-jwt';
//import { JwtHelperService } from '@auth0/angular-jwt';

//import { PersonUser, PersonUserToken } from '../../Model';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

    isLogged = new EventEmitter();

       public currentUserSubject: BehaviorSubject<Token>;
       public currentUser: Observable<Token>;

    constructor(
        private http: HttpClient,
        private router: Router
    ) {
         this.currentUserSubject = new BehaviorSubject<Token>(JSON.parse(localStorage.getItem('_quickpay:user')));
         this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): any {
        return this.currentUserSubject.value;
    }

    signIn(user: Person) {

        return this.http
            .post<Token>(`${environment.URL_API}/Auth/SignIn`, user)
            .pipe(map(res => {
                if (res && res.token) {
                    localStorage.setItem('_quickpay:user', JSON.stringify(res));
                    this.currentUserSubject.next(res);
                    this.isLogged.emit(user);
                }
                return res;
            }))
    }

    singUp(user: Person) {
        return this.http
        .post<Token>(`${environment.URL_API}/Auth/SignUp`, user)
        .pipe(map(res => {
            if (res && res.token) {
                localStorage.setItem('_quickpay:user', JSON.stringify(res));
                this.currentUserSubject.next(res);
                this.isLogged.emit(user);
            }
            return res;
        }))
    }

    logout() {
        localStorage.removeItem('token');
        this.currentUserSubject.next(null);
        this.router.navigate(['/sign-in']);
    }

      getUserFromToken() {
        const helper = new JwtHelperService();
        if (this.currentUserValue) {
          const token = this.currentUserValue;
          const decodedToken = helper.decodeToken(token.token);
          const userLogin = decodedToken as Person;
          return userLogin;
        }
      }
}
