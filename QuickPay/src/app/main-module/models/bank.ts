export class Bank {
    bankID: number;
    name: string;
    nickName: string;
    about: string;
    iconUrl: string;
    isActive: boolean;
    createdDateTime: Date;
}