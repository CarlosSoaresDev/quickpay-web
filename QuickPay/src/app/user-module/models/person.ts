export class Person {
    personID: number;
    type: number;
    name: string;
    nickName: string;
    email: string;
    password: string;
    photoUrl: string;
    isActive: boolean;
    createdDateTime: Date;
}

export class PersonUser {
    name: string;
    email: string;
    password: string;
}