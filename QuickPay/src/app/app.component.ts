import { Component } from '@angular/core';
import { ViewControllerEvent } from './common/Event/view-controller.event';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  isVisible: boolean = true;

  constructor(
    private viewControllerEvent: ViewControllerEvent
  ) {
    this.viewControllerEvent
      .isVisible
      .subscribe(res => {
        this.isVisible = res;
      });
  }
}
