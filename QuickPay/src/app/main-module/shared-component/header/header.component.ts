import { Component, OnInit } from '@angular/core';
import { Person } from 'src/app/user-module/models/person';
import { AuthenticationService } from 'src/app/user-module/services/authetication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
  user: Person;

  constructor(
    private authenticationService: AuthenticationService
  ) {
    this.user = authenticationService.getUserFromToken();
   }

  ngOnInit() {
  }

  onLogOut(): void {
    this.authenticationService
    .logout();
  }

}
