import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { take } from 'rxjs/operators';
import { Bank } from 'src/app/main-module/models/bank';
import { BankService } from 'src/app/main-module/services/bank.service';

@Component({
  selector: 'app-banks',
  templateUrl: './banks.component.html',
  styleUrls: ['./banks.component.css']
})
export class BanksComponent implements OnInit {

 //#region Property
 bankID: number;
 frmFilter: FormGroup;
 loading = false;
 banks: Bank[] = [];
 //#endregion

 //#region Constructor
 constructor(
   private formBuilder: FormBuilder,
   private bankService: BankService,
 ) { }
 //#endregion

 ngOnInit() {
   this.FilterForm();
 }

 //#region Forms
 FilterForm() {
   this.frmFilter = this.formBuilder.group({
     name: ['']    
   });
 }
 //#endregion

 //#region Services
 getAllBanks() {
   this.bankService.getAll()
     .pipe(take(1))
     .subscribe(res => {
       this.banks = res;
     }, error => {
       console.error(error)
     });
 }
 //#endregion

 //#region  Methods  
 onDeleteById() {
    this.bankService.delete(this.bankID)
        .pipe(take(1))
        .subscribe(() => {
            this.getAllBanks();
        }, error => {
          console.log(error)
        });
 }
 //#endregion
}
