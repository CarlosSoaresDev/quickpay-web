import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ViewControllerEvent } from 'src/app/common/Event/view-controller.event';
import { AuthenticationService } from '../../services/authetication.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  loading = false;
  submitted = false;
  signUpForm: FormGroup;
  user: any;

  constructor(
    private viewControllerEvent: ViewControllerEvent,
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
   // private alertService: AlertService
  ) {
    this.viewControllerEvent
      .visibleTemplate(false)
  }


  ngOnInit() {
    if (this.authenticationService.currentUserValue)
      this.router.navigate(['/']);

    this.formSingIn();
  }

  formSingIn() {
    this.signUpForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      name: ['',Validators.required],
      password: ['', Validators.required]
    });
  }

  get form() {
    return this.signUpForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.signUpForm.invalid) 
      return;
    
    this.loading = true;
    this.authenticationService
      .singUp(this.signUpForm.value)
      .subscribe(() => {
        this.router.navigate(['']);
        this.loading = false;

      }, error => {
        this.loading = false;
      //  this.alertService.error('Error de servidor, procure o suporte', '', error);
      });
  }

}

