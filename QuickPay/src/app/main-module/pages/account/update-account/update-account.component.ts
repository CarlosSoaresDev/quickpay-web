import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { take } from 'rxjs/operators';
import { Account } from 'src/app/main-module/models/Account';
import { Bank } from 'src/app/main-module/models/bank';
import { BankService } from 'src/app/main-module/services/bank.service';

@Component({
  selector: 'app-update-account',
  templateUrl: './update-account.component.html',
  styleUrls: ['./update-account.component.css']
})
export class UpdateAccountComponent implements OnInit {

  //#region Property
  acocountID: number;
  frmFilter: FormGroup;
  loading = false;
  banks: Bank[] = [];
  accounts: Account[] = [];
  //#endregion

  //#region Constructor
  constructor(
    private formBuilder: FormBuilder,
    private bankService: BankService,
    //   private accountService: AccountService,
  ) { }
  //#endregion

  ngOnInit() {
    this.FilterForm();
    //  this.getAllBanks();
  }

  //#region Forms
  FilterForm() {
    this.frmFilter = this.formBuilder.group({
      bankID: [0]
    });
  }
  //#endregion

  //#region Services
  onGetAllAccount() {
    //  this.loading = true;
    //  this.accountService.getAllFilter(this.frmFilter.value)
    //      .pipe(take(1))
    //      .subscribe(res => {
    //          this.accounts = res;
    //          this.loading = false;
    //      }, error => {             
    //          this.loading = false;
    //      });
  }

  getAllBanks() {
    this.bankService.getAll()
      .pipe(take(1))
      .subscribe(res => {
        this.banks = res;
      }, error => {
        console.error(error)
      });
  }
  //#endregion

  //#region  Methods  
  onDeleteById() {
    //  this.accountService.delete(this.acocountID)
    //      .pipe(take(1))
    //      .subscribe(() => {
    //          this.onGetAllAccount();
    //      }, error => {
    //      });
  }
  //#endregion
}

