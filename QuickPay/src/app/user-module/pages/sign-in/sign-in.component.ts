import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { ViewControllerEvent } from 'src/app/common/Event/view-controller.event';
import { AuthenticationService } from '../../services/authetication.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  loading = false;
  submitted = false;
  signInForm: FormGroup;
  user: any;

  constructor(
    private viewControllerEvent: ViewControllerEvent,
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
   // private alertService: AlertService
  ) {
    this.viewControllerEvent
      .visibleTemplate(false)
  }


  ngOnInit() {
    if (this.authenticationService.currentUserValue)
      this.router.navigate(['/']);

    this.formSingIn();
  }

  formSingIn() {
    this.signInForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required]
    });
  }

  get form() {
    return this.signInForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.signInForm.invalid) 
      return;
    
    this.loading = true;
    this.authenticationService
      .signIn(this.signInForm.value)
      .pipe(take(1))
      .subscribe(() => {
        this.router.navigate(['']);
        this.loading = false;

      }, error => {
        this.loading = false;
      //  this.alertService.error('Error de servidor, procure o suporte', '', error);
      });
  }

}



