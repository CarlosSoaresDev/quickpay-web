import { Component, OnInit } from '@angular/core';
import { ViewControllerEvent } from 'src/app/common/Event/view-controller.event';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private viewControllerEvent: ViewControllerEvent,
  ) {
    this.viewControllerEvent
      .visibleTemplate(true)
   }

  ngOnInit() {
  }

}
