import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { take } from 'rxjs/operators';
import { Account } from 'src/app/main-module/models/Account';
import { Bank } from 'src/app/main-module/models/bank';

@Component({
  selector: 'app-accouts',
  templateUrl: './accouts.component.html',
  styleUrls: ['./accouts.component.css']
})
export class AccoutsComponent implements OnInit {

   //#region Property
   acocountID: number;
   frmFilter: FormGroup;
   loading = false;
   accounts: Account[] = [];
   banks: Bank[] = [];
   //#endregion

   //#region Constructor
   constructor(
       private formBuilder: FormBuilder,
      // private bankService: BankService,
    //   private accountService: AccountService,
   ) { }
   //#endregion

   ngOnInit() {
       this.FilterForm();
     //  this.getAllBanks();
   }

   //#region Forms
   FilterForm() {
       this.frmFilter = this.formBuilder.group({
           Description: [''],
           Initial: [''],
           CountryID: [0]
       });
   }
   //#endregion

   //#region Services
   onGetAllAccount() {
      //  this.loading = true;
      //  this.accountService.getAllFilter(this.frmFilter.value)
      //      .pipe(take(1))
      //      .subscribe(res => {
      //          this.accounts = res;
      //          this.loading = false;
      //      }, error => {             
      //          this.loading = false;
      //      });
   }

   getAllBanks() {
      //  this.bankService.getAll()
      //      .pipe(take(1))
      //      .subscribe(res => {
      //          this.banks = res;
      //      }, error => {
              
      //      });
   }
   //#endregion

   //#region  Methods  
   onDeleteById() {
      //  this.accountService.delete(this.acocountID)
      //      .pipe(take(1))
      //      .subscribe(() => {
      //          this.onGetAllAccount();
      //      }, error => {
      //      });
   }
   //#endregion
}


