import { Injectable, EventEmitter } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ViewControllerEvent {

    isVisible = new EventEmitter<boolean>();

    constructor() { }

    visibleTemplate(value: boolean) {
        this.isVisible.emit(value);

    }
}
